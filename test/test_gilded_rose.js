const { expect } = require('chai')
const {
  Item,
  Shop,
  MatureItem,
  LegendaryItem,
  BackstagePass,
  ConjuredItem
} = require('../src/gilded_rose.js')

describe('Gilded Rose', function () {
  // Test for simple item
  it('Simple Dress', function () {
    const gildedRose = new Shop([])

    // Statement of a normal item
    let item = new Item('Simple Dress', 30, 10)
    gildedRose.addItem(item)

    // Item is accessible
    expect(gildedRose.items[0].name).to.equal('Simple Dress')
    expect(gildedRose.items[0].sellIn).to.equal(30)
    expect(gildedRose.items[0].quality).to.equal(10)

    // SellIn and Quality decrease by 1
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(29)
    expect(gildedRose.items[0].quality).to.equal(9)
  })

  // Test quality once sale's day has passed
  it('Gorehowl', function () {
    const gildedRose = new Shop([])
    let item = new Item('Gorehowl', 1, 3)
    gildedRose.addItem(item)

    // SellIn and Quality decrease by 1
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(0)
    expect(gildedRose.items[0].quality).to.equal(2)

    // SellIn cannot be nagative. Quality decreases by 2
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(0)
    expect(gildedRose.items[0].quality).to.equal(0)

    // Quality cannot be negative
    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(0)
  })

  // Test quality increase for the "aged brie"
  it('Aged Brie', function () {
    const gildedRose = new Shop([])
    let item = new MatureItem('Aged Brie', 30, 49)
    gildedRose.addItem(item)

    // Quality increases by 1
    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(50)

    // Quality cannot exceed 50
    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(50)
  })

  // Test for legendary items
  it('Sulfuras, Hand of Ragnaros', function () {
    const gildedRose = new Shop([])
    let item = new LegendaryItem('Sulfuras, Hand of Ragnaros', 30, 25)
    gildedRose.addItem(item)

    // Declared unsalable and with a quality of 80
    expect(gildedRose.items[0].sellIn).to.equal(0)
    expect(gildedRose.items[0].quality).to.equal(80)

    // Doesn't change with an update
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(0)
    expect(gildedRose.items[0].quality).to.equal(80)
  })

  it('Backstage passes to a TAFKAL80ETC concert', function () {
    const gildedRose = new Shop([])
    let item = new BackstagePass('Backstage passes to a TAFKAL80ETC concert', 11, 0)
    gildedRose.addItem(item)

    // Under SellIn at 5, quality increases by 2
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(10)
    expect(gildedRose.items[0].quality).to.equal(2)

    // Under SellIn at 5, quality increases by 3
    gildedRose.items[0].sellIn = 6
    gildedRose.updateQuality()
    expect(gildedRose.items[0].sellIn).to.equal(5)
    expect(gildedRose.items[0].quality).to.equal(5)

    // At SellIn equal 0, quality decreases to 0
    gildedRose.items[0].sellIn = 0
    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(0)
  })

  // Test for conjured items
  it('Atiesh, Greatstaff of the Guardian', function () {
    const gildedRose = new Shop([])
    let item = new ConjuredItem('Atiesh, Greatstaff of the Guardian', 1, 60)
    gildedRose.addItem(item)

    // Decreases twice as fast than a normal item
    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(58)

    gildedRose.updateQuality()
    expect(gildedRose.items[0].quality).to.equal(54)
  })
})
