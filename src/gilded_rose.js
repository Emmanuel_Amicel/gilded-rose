// Statement of the super class "Item"
class Item {
  constructor (name, sellIn, quality) {
    this.name = name
    this.sellIn = sellIn
    this.quality = quality
  }

  decreaseSellIn (num) {
    let res = this.sellIn - num
    if (res < 0) {
      this.sellIn = 0
    } else {
      this.sellIn = res
    }
  }
  decreaseQuality (num) {
    let res = this.quality - num
    if (res < 0) {
      this.quality = 0
    } else {
      this.quality = res
    }
  }
  increaseQuality (num) {
    let res = this.quality + num
    if (res > 50) {
      this.quality = 50
    } else {
      this.quality = res
    }
  }

  updateQuality () {
    if (this.sellIn > 0) {
      this.decreaseSellIn(1)
      this.decreaseQuality(1)
    } else {
      this.decreaseQuality(2)
    }
  }
}

// Statement of the "Aged Brie" class
class MatureItem extends Item {
  updateQuality () {
    if (this.sellIn > 0) {
      this.decreaseSellIn(1)
      this.increaseQuality(1)
    } else {
      this.increaseQuality(2)
    }
  }
}

// Statement of the "Sulfuras" class
class LegendaryItem extends Item {
  constructor (name, sellIn, quality) {
    super(name, sellIn, quality)
    this.sellIn = 0
    this.quality = 80
  }

  updateQuality () {}
}

// Statement of the "Backstage Pass" class
class BackstagePass extends Item {
  updateQuality () {
    this.decreaseSellIn(1)
    if (this.sellIn > 10) {
      this.increaseQuality(1)
    } else {
      this.increaseQuality(2)
    }

    if (this.sellIn <= 5) {
      this.increaseQuality(1)
    }
    if (this.sellIn <= 0) {
      this.quality = 0
    }
  }
}

// Statement of "Conjured Item" class
class ConjuredItem extends Item {
  updateQuality () {
    if (this.sellIn > 0) {
      this.decreaseSellIn(1)
      this.decreaseQuality(2)
    } else {
      this.decreaseQuality(4)
    }
  }
}

// Statement for the shop
class Shop {
  constructor (items = []) {
    this.items = items
  }

  addItem (item) {
    this.items.push(item)
  }

  updateQuality () {
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].updateQuality()
    }
  }
}

module.exports = {
  Item,
  Shop,
  MatureItem,
  LegendaryItem,
  BackstagePass,
  ConjuredItem
}
