# Gilded Rose

Refactoring Kata of the Gilded Rose selling system, by Emmanuel Amicel. 

How to compile this code :
After downloading the code in an empty folder, run in a Windowns terminal:

  npm install

To run tests, use :

  npm test